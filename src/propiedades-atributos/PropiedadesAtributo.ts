import { Propiedad } from "./Propiedad";

export class PropiedadesAtributo {
    private nombre: string;
    private constrains: Propiedad[];
    constructor() {
        this.nombre = '';
        this.constrains = [];
    }

    getConstrains() {
        return this.constrains;
    }

    getNombre() {
        return this.nombre;
    }
}