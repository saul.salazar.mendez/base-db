import { Propiedad } from "./Propiedad";
import { tipoError } from "./TextoTiposError";

export class Notnull extends Propiedad {
    constructor(mensajeError: string = ''){
        super();
        this.tipo = 'not null';
        this.mensajeError = mensajeError.length > 0 ? 
            mensajeError : tipoError.notNull();
    }
    public valida(valor: any): boolean {
        if (valor !== null && valor !== undefined && valor !== ''){
            return true;
        }
        return false;
    }
}