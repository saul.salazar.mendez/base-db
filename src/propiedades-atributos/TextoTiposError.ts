export const tipoError = {
    minLen(valor: number){;
        return `Error: Necesita escribir al menos ${valor} carácteres`;
    },
    maxLen(valor: number){;
        return `Error: Solo puede escribir ${valor} carácteres`;
    },
    minValue(valor: number){;
        return `Error: El valor debe ser mayor o igual a ${valor}`;
    },
    maxValue(valor: number){;
        return `Error: El valor debe ser menor o igual a ${valor}`;
    },
    notNull(){
        return `Error: Este campo es obligatorio`;
    },
    regex(){
        return 'Error: No se cumple con la condición establecida';
    }
}