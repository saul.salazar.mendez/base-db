import { Propiedad } from "./Propiedad";
import { tipoError } from "./TextoTiposError";

export class Regex extends Propiedad {
    private _valor: string;
    public get valor(): string {
        return this._valor;
    }
    public set valor(value: string) {
        this._valor = value;
    }
    constructor(valor: string, mensajeError: string = '') {
        super();
        this._valor = valor;
        this.tipo = 'regex';
        this.mensajeError = mensajeError.length > 0 ? 
            mensajeError : tipoError.regex();
    }
    public valida(valor: string): boolean {
        if (valor !== null && valor !== undefined && valor !== ''){
            const reg = new RegExp(this.valor);
            return reg.test(valor);
        }
        return false;
    }
}