import { Propiedad } from "../propiedades-atributos/Propiedad";

export class Atributo {
    
    constructor(nombre: string, propiedades: Propiedad[] = []){
        this._nombre = nombre;
        this._propiedades = propiedades;
        this._tipo = 'sin tipo';
    }
    
    private _nombre: string;
    private _propiedades: Propiedad[];
    private _tipo: string;

    public get tipo(): string {
        return this._tipo;
    }
    public set tipo(value: string) {
        this._tipo = value;
    }
    public get nombre(): string {
        return this._nombre;
    }
    public set nombre(value: string) {
        this._nombre = value;
    }
    public get propiedades(): Propiedad[] {
        return this._propiedades;
    }
    public set propiedades(value: Propiedad[]) {
        this._propiedades = value;
    }
    public getError(valor: any): string {
        for(const propiedad of this._propiedades) {
            if (!propiedad.valida(valor)) {
                return propiedad.mensajeError;
            }
        }
        return '';
    }
    public tieneError(valor: any): boolean {
        for(const propiedad of this._propiedades) {
            if (!propiedad.valida(valor)) {
                return true;
            }
        }
        return false;
    }
}