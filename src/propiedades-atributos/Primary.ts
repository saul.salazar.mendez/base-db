import { Propiedad } from "./Propiedad";

export class Primary extends Propiedad {
    constructor() {
        super();
        this.tipo = 'primary';
    }
}