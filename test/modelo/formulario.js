var assert = require('assert');
var Formulario = require('../../dist/modelo/formulario').Formulario;
var Texto = require('../../dist/atributos/Texto').Texto;
var Notnull = require('../../dist/propiedades-atributos/Notnull').Notnull;
describe('Formulario', function() {
    describe('Validar campo requerido', function() {
        it('Validacion cuando es requerido y se tiene el valor', function() {
            let formulario = new Formulario('Persona');
            formulario.addAtributo(new Texto('nombre', [new Notnull()]));
            const validacion = formulario.valida({nombre: 'juan'});
            assert.equal(validacion.nombre.tieneError, false);
        });
        it('Validacion cuando es requerido y falta el valor', function() {
            let formulario = new Formulario('Persona');
            formulario.addAtributo(new Texto('nombre', [new Notnull()]));
            const validacion = formulario.valida({nombre: ''});
            assert.equal(validacion.nombre.tieneError, true);
        });
    });
});