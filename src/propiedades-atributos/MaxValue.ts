import { Propiedad } from "./Propiedad";
import { tipoError } from "./TextoTiposError";

export class MaxValue extends Propiedad {
    private _valor: number;
    public get valor(): number {
        return this._valor;
    }
    public set valor(value: number) {
        this._valor = value;
    }
    constructor(valor: number, mensajeError: string = '') {
        super();
        this._valor = valor;
        this.tipo = 'max value';
        this.mensajeError = mensajeError.length > 0 ? 
            mensajeError : tipoError.maxValue(valor);
    }
    public valida(valor: number): boolean {
        if(typeof valor === 'number'){
            return valor <= this.valor ? true : false;
        }
        return false;
    }
}