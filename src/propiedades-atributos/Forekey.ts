import { Propiedad } from "./Propiedad";

export class Forekey extends Propiedad{
    constructor(tabla:string, key:string) {
        super();
        this._tabla = tabla;
        this._key = key;
        this.tipo = 'forekey';
    }

    private _tabla: string;
    private _key: string;
    public get tabla(): string {
        return this._tabla;
    }
    public set tabla(value: string) {
        this._tabla = value;
    }
    public get key(): string {
        return this._key;
    }
    public set key(value: string) {
        this._key = value;
    }
    
}