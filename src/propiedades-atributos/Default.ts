import { Propiedad } from "./Propiedad";

export class Default extends Propiedad {
    private _valor: any;
    public get valor(): any {
        return this._valor;
    }
    public set valor(value: any) {
        this._valor = value;
    }
    constructor(valor: any) {
        super();
        this.valor = valor;
        this.tipo = 'default';
    }
}