import { Atributo } from "./Atributo";
import { Propiedad } from "../propiedades-atributos/Propiedad";

export class Real extends Atributo{
    constructor(nombre: string, propiedades: Propiedad[] = []) {
        super(nombre, propiedades);
        this.tipo = 'real';
    }
}