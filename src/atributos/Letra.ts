import { Atributo } from "./Atributo";
import { Propiedad } from "../propiedades-atributos/Propiedad";

export class Letra extends Atributo{
    constructor(nombre: string, propiedades: Propiedad[] = []) {
        super(nombre, propiedades);
        this.tipo = 'letra';
    }
}