import { Propiedad } from "./Propiedad";
import { tipoError } from "./TextoTiposError";

export class MaxLen extends Propiedad {
    private _valor: number;
    public get valor(): number {
        return this._valor;
    }
    public set valor(value: number) {
        this._valor = value;
    }
    constructor(valor: number, mensajeError: string = '') {
        super();
        this._valor = valor;
        this.tipo = 'max len';
        this.mensajeError = mensajeError.length > 0 ? 
            mensajeError : tipoError.maxLen(valor);
    }
    public valida(valor: string): boolean {
        if(typeof valor === 'string'){
            return valor.length <= this.valor ? true : false;
        }
        return false;
    }
}