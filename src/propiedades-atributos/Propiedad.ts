export class Propiedad{
    constructor() {
        this._tipo = 'sin tipo';
        this._mensajeError = 'Hay error';
    }
    private _tipo: string;
    private _mensajeError: string;
    public get mensajeError(): string {
        return this._mensajeError;
    }
    public set mensajeError(value: string) {
        this._mensajeError = value;
    }
    public get tipo(): string {
        return this._tipo;
    }
    public set tipo(value: string) {
        this._tipo = value;
    }
    public valida(valor: any = null): boolean{
        return true;
    }
}