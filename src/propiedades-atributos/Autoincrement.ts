import { Propiedad } from "./Propiedad";

export class Autoincrement extends Propiedad{
    constructor() {
        super();
        this.tipo = 'autoincrement';
    }
}