import { Tabla } from "./tabla";

interface ValidacionAtributo {
    tieneError: boolean,
    mensajeError: string
}

interface ValidacionFormulario {
    [key: string]: ValidacionAtributo
}

export class Formulario extends Tabla{    
    constructor(nombre: string) {        
        super(nombre);        
    }
    public valida(datos: any): ValidacionFormulario{
        let salida:ValidacionFormulario = {};
        for(const llave in this.atributos){
            const atributo = this.atributos[llave];
            atributo.tieneError(datos[llave]);
            salida[llave] = {
                tieneError: this.atributos[llave].tieneError(datos[llave]),
                mensajeError: this.atributos[llave].getError(datos[llave])
            }
        }
        return salida;
    }
    public validaAtributo(campo:string, valor: any): ValidacionAtributo{
        return {
            tieneError: this.atributos[campo].tieneError(valor),
            mensajeError: this.atributos[campo].getError(valor)
        }
    }
}