import { Atributo } from "../atributos/Atributo";

interface Atributos {
    [key: string]: Atributo
}

export class Tabla{
    private _atributos: Atributos;
    public get nombre(): string {
        return this._nombre;
    }
    public set nombre(value: string) {
        this._nombre = value;
    }    
    public get atributos(): Atributos {
        return this._atributos;
    }
    constructor(private _nombre: string) {        
        this._atributos = {};
    }

    addAtributo(atributo: Atributo) {
        this._atributos[atributo.nombre] = atributo;
    }    
}